terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
  description = "api key"
  sensitive = true
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "mac16" {
  name = "mac book pro 16"
}

resource "digitalocean_droplet" "web-terraform-homework-01" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.mac16.id]
}

resource "digitalocean_droplet" "web-terraform-homework-02" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.mac16.id]
}

resource "digitalocean_loadbalancer" "loadbalancer-1" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path = "/"
  }

  droplet_ids = [digitalocean_droplet.web-terraform-homework-01.id, digitalocean_droplet.web-terraform-homework-02.id]
}

resource "digitalocean_record" "terraform" {
  domain = "victor-zhukovski.club"
  type   = "A"
  name   = "@"
  value  = digitalocean_loadbalancer.loadbalancer-1.ip
}
