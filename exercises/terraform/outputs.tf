output "loadbalancer_ip" {
  value = digitalocean_loadbalancer.loadbalancer-1.ip
  description = "loadbalancer ip"
}

output "sever-1" {
  value = digitalocean_droplet.web-terraform-homework-01.ipv4_address
  description = "server 1 ip"
}

output "server-2" {
  value = digitalocean_droplet.web-terraform-homework-02.ipv4_address
  description = "server 2 ip"
}