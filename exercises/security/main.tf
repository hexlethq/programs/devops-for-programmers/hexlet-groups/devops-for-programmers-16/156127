terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
  description = "api key"
  sensitive = true
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "mac16" {
  name = "mac book pro 16"
}


resource "digitalocean_vpc" "security_vpc" {
  name     = "security-task"
  region   = "ams3"
}

resource "digitalocean_droplet" "server" {
  count = 2
  image    = "docker-20-04"
  name     = format("security-%02d", count.index + 1)
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.mac16.id]
  private_networking = true
  vpc_uuid = digitalocean_vpc.security_vpc.id
}

resource "digitalocean_droplet" "bastion" {
  image              = "docker-20-04"
  name               = "bastion"
  region             = "ams3"
  size               = "s-1vcpu-1gb"
  private_networking = true
  vpc_uuid           = digitalocean_vpc.security_vpc.id
  ssh_keys = [data.digitalocean_ssh_key.mac16.id]
}

resource "digitalocean_certificate" "security-certificate" {
  name    = "domain-cert"
  type    = "lets_encrypt"
  domains = ["security.victor-zhukovski.club"]
}



resource "digitalocean_loadbalancer" "security_loadbalancer" {
  name     = "security-balancer"
  region   = "ams3"
  vpc_uuid = digitalocean_vpc.security_vpc.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"

    certificate_name = digitalocean_certificate.security-certificate.name
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.server[*].id
}

resource "digitalocean_record" "security-task-record" {
  domain = "victor-zhukovski.club"
  type   = "A"
  name   = "security"
  value  = digitalocean_loadbalancer.security_loadbalancer.ip
}

resource "digitalocean_firewall" "bastion" {
  name        = "bastion-firewall"
  droplet_ids = [digitalocean_droplet.bastion.id]


  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol              = "icmp"
    source_droplet_ids = digitalocean_droplet.server[*].id
  }

  outbound_rule {
    protocol              = "tcp"
    port_range       = "22"
    destination_droplet_ids = digitalocean_droplet.server[*].id
  }

  outbound_rule {
    protocol              = "icmp"
    destination_droplet_ids = digitalocean_droplet.server[*].id
  }
}


resource "digitalocean_firewall" "server" {
  name = "server-firewall"
  droplet_ids = digitalocean_droplet.server[*].id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "53"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "5000"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  inbound_rule {
    protocol              = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol              = "tcp"
    port_range = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol              = "tcp"
    port_range = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}