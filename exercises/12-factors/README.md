
# Приложение 12 факторов

Деплоим на Heroku [strapi](https://github.com/strapi/strapi).

## Ссылки

* [The Twelve-Factor App](https://12factor.net/)
* [Strapi](https://github.com/strapi/strapi)
* [Quick Start Guide](https://strapi.io/documentation/developer-docs/latest/getting-started/quick-start.html)
* [Getting Started on Heroku with Node.js](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
* [Конфигурация Strapi](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html)
* [Deployment Strapi app to Heroku](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment/hosting-guides/heroku.html)

## Задание

1. Создайте в директории *app* приложение Strapi согласно [документации](https://strapi.io/documentation/developer-docs/latest/getting-started/quick-start.html#_1-install-strapi-and-create-a-new-project)
1. Установите [Heroku Cli](https://devcenter.heroku.com/articles/heroku-cli)
1. Задеплойте приложение с помощью heroku cli. Убедитесь, что приложение конфигурируется с помощью переменных окружения.
1. Когда будете отправлять задание на проверку, то укажите в issue ссылку на задеплоенное приложение.
