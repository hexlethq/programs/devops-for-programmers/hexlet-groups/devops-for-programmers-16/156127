terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  sensitive   = true
  type        = string
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "mac16" {
  name = "mac book pro 16"
}

resource "digitalocean_droplet" "afs_server" {
  count = 2
  image    = "ubuntu-20-10-x64"
  name     = format("afs-server-%02d", count.index + 1)
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.mac16.id]
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-afs"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path = "/"
  }

  droplet_ids = digitalocean_droplet.afs_server[*].id
}

resource "digitalocean_record" "asf-task-record" {
  domain = "victor-zhukovski.club"
  type   = "A"
  name   = "afs"
  value  = digitalocean_loadbalancer.public.ip
}
