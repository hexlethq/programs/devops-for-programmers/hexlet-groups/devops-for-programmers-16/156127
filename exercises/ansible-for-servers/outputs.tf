output "droplets" {
  value = digitalocean_droplet.afs_server[*]
}

output "ssh_info" {
  value = data.digitalocean_ssh_key.mac16
}